# Purpose
This repository is for creating a LaTeX package for including Tiger
code. It basically just defines the language so that listings can
perform syntax highlighting.

# Author
The main author is Casper Freksen.
